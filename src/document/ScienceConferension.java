package document;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class ScienceConferension extends Document {
	private Set<String> actions=new TreeSet<String>();
	
	public ScienceConferension(String actions,int aud,
			LocalDateTime prosecutionDate) {
		String[] s = actions.split("\n");
		for (String s1 : s)
			if (!s1.isEmpty())
				this.actions.add(s1);
		this.prosecutionDate = prosecutionDate;
		this.audNumber = aud;
	}

	public Set<String> getActions() {
		return actions;
	}

	public void setActions(Set<String> actions) {
		this.actions = actions;
	}

	@Override
	public void writeToWord(String name) {
		int count = 1;
		XWPFDocument document = new XWPFDocument();
		String title = "�������� ����� ���� ���������� ���������� �� �����������";
		writeHeader(document, title);
		XWPFParagraph paragraph1 = document.createParagraph();
		paragraph1.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun run1 = paragraph1.createRun();
		run1.setFontSize(22);
		run1.setText("������� ������");
		run1.addBreak();
		for (String action : this.actions) {
			XWPFParagraph paragraph2 = document.createParagraph();
			paragraph2.setAlignment(ParagraphAlignment.LEFT);
			paragraph2.setSpacingAfter(0);
			XWPFRun run5 = paragraph2.createRun();
			run5.setFontSize(15);
			if (!action.substring(0, 2).matches("[0-9]."))
				run5.setText(count + ". " + action);
			else
				run5.setText(action);
			run5.addBreak();
			count++;
		}

		writeFooter(document, name);

	}

	@Override
	public void writeFooter(XWPFDocument document, String name) {
		XWPFParagraph paragraph = document.createParagraph();
		paragraph.setAlignment(ParagraphAlignment.CENTER);
		paragraph.setSpacingAfter(0);
		paragraph.setBorderBottom(Borders.BASIC_THIN_LINES);
		XWPFRun run4 = paragraph.createRun();
		run4.setFontSize(15);
		run4.setItalic(true);
		run4.setText("������������ �� ����������� ���������� \\n �������");

		try {
			FileOutputStream output = new FileOutputStream(name + ".docx");
			document.write(output);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	};

}
