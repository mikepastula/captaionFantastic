package document;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public abstract class Document {
	protected LocalDateTime prosecutionDate;
	protected int audNumber;
    
	public LocalDateTime getProsecutionDate() {
		return prosecutionDate;
	}

	public void setProsecutionDate(LocalDateTime prosecutionDate) {
		this.prosecutionDate = prosecutionDate;
	}

	public int getAudNumber() {
		return audNumber;
	}

	public void setAudNumber(int audNumber) {
		this.audNumber = audNumber;
	}

	public void writeHeader(XWPFDocument document, String title) {
		XWPFParagraph paragraph1 = document.createParagraph();
		XWPFRun run1 = paragraph1.createRun();
		paragraph1.setAlignment(ParagraphAlignment.CENTER);
		paragraph1.setSpacingAfter(100);
		run1.setFontSize(25);
		run1.setBold(true);
		run1.setText("����������");
		run1.addBreak();

		XWPFRun run2 = paragraph1.createRun();
		run2.setFontSize(15);
		run2.setBold(false);
		run2.setText(Integer.toString(prosecutionDate.getDayOfMonth()));
		switch (prosecutionDate.getMonthValue()) {
		case 1:
			run2.setText(" ���� " + prosecutionDate.getYear() + " ���� �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 2:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 3:
			run2.setText(" ������� " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 4:
			run2.setText(" ����� " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 5:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ���� �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 6:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 7:
			run2.setText(" ����� " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 8:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 9:
			run2.setText(" ������� " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 10:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 11:
			run2.setText(" ��������� " + prosecutionDate.getYear() + " ����  �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		case 12:
			run2.setText(" ������ " + prosecutionDate.getYear() + " ���� �" + prosecutionDate.getHour() + "."
					+ prosecutionDate.getMinute());
			break;
		}
		run2.setText(" � ��� �" + audNumber + " ����������");
		run2.addBreak();
		run2.setText(title);

	}

	public void writeFooter(XWPFDocument document, String name) {
		XWPFParagraph paragraph = document.createParagraph();
		paragraph.setAlignment(ParagraphAlignment.CENTER);
		paragraph.setSpacingAfter(0);
		paragraph.setBorderBottom(Borders.BASIC_THIN_LINES);
		XWPFRun run4 = paragraph.createRun();
		run4.setFontSize(15);
		run4.setItalic(true);
		run4.setText("������������ �� �������");

		try {
			FileOutputStream output = new FileOutputStream(name + ".docx");
			document.write(output);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	};

	public abstract void writeToWord(String name);

}
