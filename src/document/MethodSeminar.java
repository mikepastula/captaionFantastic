package document;

import java.time.LocalDateTime;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class MethodSeminar extends Document {
	private String theme;
	private String speaker;

	public MethodSeminar(String theme, String speaker, int audNumber, LocalDateTime date) {
		this.prosecutionDate = date;
		this.audNumber = audNumber;
		this.theme = theme;
		this.speaker = speaker;
	}	
	
	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	@Override
	public void writeToWord(String name) {
		XWPFDocument document = new XWPFDocument();
		String title = "���������� ������ ������� ��������� ���������� �� ������������� ���������";
		writeHeader(document, title);

		XWPFParagraph paragraph2 = document.createParagraph();
		paragraph2.setAlignment(ParagraphAlignment.CENTER);
		paragraph2.setSpacingAfter(0);
		XWPFRun run5 = paragraph2.createRun();
		run5.setFontSize(15);
		run5.setText(theme);
		run5.addBreak();
		run5.addBreak();

		XWPFParagraph paragraph3 = document.createParagraph();
		paragraph3.setAlignment(ParagraphAlignment.LEFT);
		paragraph3.setSpacingAfter(0);
		XWPFRun run3 = paragraph3.createRun();
		run3.setFontSize(15);
		run3.setText("�������� : " + this.speaker);
		run3.addBreak();
		run3.addBreak();

		XWPFParagraph paragraph4 = document.createParagraph();
		paragraph4.setAlignment(ParagraphAlignment.CENTER);
		paragraph4.setSpacingAfter(0);
		paragraph4.setBorderBottom(Borders.BASIC_THIN_LINES);
		writeFooter(document, name);
	}

}
