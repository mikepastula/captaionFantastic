package document;

import java.time.LocalDateTime;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class ScienceSeminar extends Document {
	private String theme;
	private String speaker;
	private String scienceLeader;

	public ScienceSeminar(String theme, String speaker, String scienceLeader, int audNumber, LocalDateTime date) {
		this.prosecutionDate = date;
		this.audNumber = audNumber;
		this.theme = theme;
		this.speaker = speaker;
		this.scienceLeader=scienceLeader;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSpeaker() {
		return speaker;
	}

	public void setSpeaker(String speaker) {
		this.speaker = speaker;
	}

	public String getScienceLeader() {
		return scienceLeader;
	}

	public void setScienceLeader(String scienceLeader) {
		this.scienceLeader = scienceLeader;
	}

	@Override
	public void writeToWord(String name) {
		XWPFDocument document = new XWPFDocument();
		String theme = "�������� ������ ������� ��������� ���������� �� ������������� ���������";
		writeHeader(document, theme);

		XWPFParagraph paragraph3 = document.createParagraph();
		paragraph3.setAlignment(ParagraphAlignment.LEFT);
		paragraph3.setSpacingAfter(0);
		XWPFRun run3 = paragraph3.createRun();
		run3.setFontSize(15);
		run3.setText("�������� : " + this.speaker);
		run3.addBreak();
		run3.setText("���� ������ : " + this.theme);
		run3.addBreak();
		run3.setText("�������� ������� : " + this.scienceLeader);
		run3.addBreak();

		writeFooter(document, name);

	}

}
