package model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Lesson {
	StringProperty day;
	StringProperty lesson;
	StringProperty lessonName;
	StringProperty group;
	StringProperty aud;
	StringProperty type;

	public StringProperty getGroupProperty() {
		return group;
	}
	
	public StringProperty getAudProperty() {
		return aud;
	}
	
	public StringProperty getNameProperty() {
		return lessonName;
	}
	
	public StringProperty getLessonProperty() {
		return lesson;
	}
	
	public StringProperty getTypeProperty() {
		return type;
	}

	public StringProperty getDayProperty() {
		return day;
	}

	public String getType() {
		return type.get();
	}

	public void setType(String type) {
		this.type = new SimpleStringProperty(type);
	}

	public String getDay() {
		return day.get();
	}

	public void setDay(String day) {
		this.day = new SimpleStringProperty(day);
	}

	public String getLesson() {
		return lesson.get();
	}

	public void setLesson(String lesson) {
		this.lesson = new SimpleStringProperty(lesson);
	}

	public String getLessonName() {
		return lessonName.get();
	}

	public void setLessonName(String lessonName) {
		this.lessonName = new SimpleStringProperty(lessonName);
	}

	public String getGroup() {
		return group.get();
	}

	public void setGroup(String group) {
		this.group = new SimpleStringProperty(group);
	}

	public String getAud() {
		return aud.get();
	}

	public void setAud(String aud) {
		this.aud = new SimpleStringProperty(aud);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aud == null) ? 0 : aud.hashCode());
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((lesson == null) ? 0 : lesson.hashCode());
		result = prime * result + ((lessonName == null) ? 0 : lessonName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		Lesson other = (Lesson) obj;
		if (lessonName == null) {
			if (other.lessonName != null)
				return false;
		} else if (!lessonName.equals(other.lessonName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Lesson [day=" + day + ", lesson=" + lesson + ", lessonName=" + lessonName + ", group=" + group
				+ ", aud=" + aud + ", type=" + type + "]";
	}

}
