package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


public class UserSerializer {
		public void serialize(Collection<User> objects, File file) throws IOException {
			ObjectMapper objectMapper = new ObjectMapper();
	        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	        objectMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
	        StringWriter stringDep = new StringWriter();
	        objectMapper.writeValue(stringDep, objects);
	        FileWriter fw = new FileWriter(file);
	        fw.write(stringDep.toString());
	        fw.close();
		}

		public List<User> deserialize(File file) throws IOException {
			 ObjectMapper mapper = new ObjectMapper();
			 
		return mapper.readValue(file,mapper.getTypeFactory().constructCollectionType(List.class, User.class));
		}
}
