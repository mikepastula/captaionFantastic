package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {
	private StringProperty firstname;
	private StringProperty lastname;
	private StringProperty secondname;
	private StringProperty role;
	private StringProperty email;
	private StringProperty password;
	private BooleanProperty send;

	public String getFirstname() {
		return firstname.get();
	}

	public String getSecondname() {
		return secondname.get();
	}

	public void setSecondname(String secondname) {
		this.secondname = new SimpleStringProperty(secondname);
	}

	public void setFirstname(String firstname) {
		this.firstname = new SimpleStringProperty(firstname);
	}

	public String getLastname() {
		return lastname.get();
	}

	public void setLastname(String lastname) {
		this.lastname = new SimpleStringProperty(lastname);
	}

	public String getRole() {
		return role.get();
	}

	public void setRole(String role) {
		this.role = new SimpleStringProperty(role);
	}

	public String getEmail() {
		return email.get();
	}

	public void setEmail(String email) {
		this.email = new SimpleStringProperty(email);
	}

	public String getPassword() {
		return password.get();
	}

	public void setPassword(String password) {
		this.password = new SimpleStringProperty(password);
	}

	public void setSend(boolean send) {
		this.send = new SimpleBooleanProperty(send);
	}

	public boolean getSend() {
		return this.send.get();
	}

	@JsonIgnore
	@Override
	public String toString() {
		return "User [firstname=" + firstname + ", lastname=" + lastname + ", role=" + role + ", email=" + email
				+ ", password=" + password + "]";
	}
}
