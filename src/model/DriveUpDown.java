package model;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files.Create;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Collections;

public class DriveUpDown {
	private static final String APPLICATION_NAME = "rozklad";

	private static final java.io.File DATA_STORE_DIR = new java.io.File("D:/.store/drive_sample");
	private static final String UPLOAD_FILE_PATH =  "D:/chnuPM/";
	private static final String DIR_FOR_DOWNLOADS = "D:/chnuPM/";
	private static FileDataStoreFactory dataStoreFactory;
	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static Drive drive;
	
	public DriveUpDown() {
		try {
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
			Credential credential = authorize();
			drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
					.build();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	private static Credential authorize() throws Exception {
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(DriveUpDown.class.getResourceAsStream("/client_secrets.json")));
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY,
				clientSecrets, Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(dataStoreFactory).build();
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}

	public File uploadFile(String type,String name) throws IOException {
		File fileMetadata = new File();
		fileMetadata.setName(name);
		FileContent mediaContent = new FileContent(type,new java.io.File(UPLOAD_FILE_PATH+name));
		Create insert = drive.files().create(fileMetadata, mediaContent);
		return insert.execute();
	}

	/** Updates uploaded file */
	public File updateFile(String name) throws IOException {
		File fileMetadata = new File();
		fileMetadata.setName(name);
		FileList files = drive.files().list().execute();
		for (File f : files.getFiles()) {
			if (f.getName().equals(name)) {
				Drive.Files.Update update = drive.files().update(f.getId(), fileMetadata);
				return update.execute();
			}
		}
		return null;
	}

	/** Downloads a file*/
	public boolean downloadFile(String name) throws IOException {
		java.io.File parentDir = new java.io.File(DIR_FOR_DOWNLOADS);
		if (!parentDir.exists() && !parentDir.mkdirs()) {
			throw new IOException("Unable to create parent directory");
		}
		FileList files = drive.files().list().setFields("nextPageToken, files(id, name, webContentLink,webViewLink)")
				.execute();
		for (File f : files.getFiles()) {
			if (f.getName().equals(name)) {
				if(!f.getName().endsWith(".json") && !f.getModifiedTime().isDateOnly())
					return false;
				OutputStream out = new FileOutputStream(new java.io.File(parentDir, name));
				drive.files().get(f.getId()).executeMediaAndDownloadTo(out);
				return true;
			}
		}
		return false;
	}

}
