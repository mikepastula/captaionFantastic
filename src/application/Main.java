package application;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.DriveUpDown;
import model.User;
import model.UserSerializer;
import view.AdvertisementController;
import view.EmailSendController;
import view.FileOverViewController;
import view.LoginController;
import view.TeacherController;
import view.TeacherOverviewController;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;

	private List<File> files = new ArrayList<>();
	private List<User> personData = new ArrayList<>();
	private User current = new User();

	public Main() {
	}

	@Override
	public void start(Stage primaryStage) {

		try {
			//DriveUpDown d =new DriveUpDown();
			//d.downloadFile("user.json");
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource(
				// "D:/chnuPM/user.json"
					"resource/users.json").getFile());
			if (file.exists()) {
				loadPersonDataFromFile(file);
			}
			for (User user : personData) {
				if (user.getRole().equals("user"))
					user.setSend(true);
			}
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle("��������� ������ �� ������ ��������� ����������");
			initRootLayout();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/view/Login.fxml"));
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			LoginController controller = loader.getController();
			controller.setMainApp(this);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showEmail() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/view/Email.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("³�������� ����������� ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			EmailSendController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showTeacher() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/view/Teacher.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("������� ");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			TeacherController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showAdvertisement(String name) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource(name));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("����������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			AdvertisementController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showTeacherOverview() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/view/TeacherOverview.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("���������");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			TeacherOverviewController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showFilesOverview() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/view/FilesOverview.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle("�����");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			FileOverViewController controller = loader.getController();
			controller.setMainApp(this);
			dialogStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadPersonDataFromFile(File file) {
		personData.clear();
		try {
			personData = new UserSerializer().deserialize(file);
		} catch (Exception e) {
			e.printStackTrace();
			User u = new User();
			u.setFirstname("Admin");
			u.setLastname("Admin");
			u.setPassword("1");
			u.setEmail("1");
			u.setRole("admin");
			u.setSend(false);
			u.setSecondname("Admin");
			personData.add(u);
			savePersonDataToFile();
		}
	}

	public void savePersonDataToFile() {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("resource/users.json").getFile());
			new UserSerializer().serialize(personData, file);
			System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<User> getUsers() {
		return personData;
	}

	public void addUser(User user) {
		personData.add(user);
	}

	public List<File> getFiles() {
		return files;
	}

	public void addFile(File file) {
		files.add(file);
	}

	public void setCurrentUser(User user) {
		this.current = user;
	}

	public User getCurrentUser() {
		return current;
	}

	public void deleteUser(int id) {
		personData.remove(id);
	}

	public void deleteFile(int id) {
		files.remove(id);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
