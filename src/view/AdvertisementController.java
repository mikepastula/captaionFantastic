package view;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import document.OpenLesson;
import document.ScienceConferension;
import document.ScienceSeminar;
import document.DepartmentSession;
import document.MethodSeminar;
import document.CompanySeminar;
public class AdvertisementController {
	@FXML
	private TextField teacher;
	@FXML
	private Label message;
	@FXML
	private TextField groups;
	@FXML
	private TextField theme;
	@FXML
	private TextArea about;
	@FXML
	private TextField course;
	@FXML
	private TextField aud;
	@FXML
	private TextField time;
	@FXML
	private TextField scienceLeader;
	@FXML
	private DatePicker date;
	@FXML
	private DatePicker writeDate;
	@FXML
	private TextField protocol;

	private Main mainApp;
	
	public AdvertisementController() {
	}

	@FXML
	private void initialize() {
		date.setValue(LocalDate.now());
		date.setShowWeekNumbers(true);
	}

	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
	}

	@FXML
	private void OpenLesson(ActionEvent event) {
		if (teacher.getText().isEmpty() || groups.getText().isEmpty() || theme.getText().isEmpty()
				|| course.getText().isEmpty() || aud.getText().isEmpty() || time.getText().isEmpty()) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			OpenLesson document = new OpenLesson(this.theme.getText(), this.teacher.getText(), this.groups.getText(),
					this.course.getText(), Integer.valueOf(this.aud.getText()), date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "³������� ���� " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "³������� ���� " + this.date.getValue()));
		}
	}

	@FXML
	private void ScieneSeminar(ActionEvent event) {
		if (teacher.getText().isEmpty() || scienceLeader.getText().isEmpty() || theme.getText().isEmpty()
				|| aud.getText().isEmpty() || time.getText().isEmpty()) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			ScienceSeminar document = new ScienceSeminar(this.theme.getText(), this.teacher.getText(),
					this.scienceLeader.getText(), Integer.valueOf(this.aud.getText()), date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "�������� ������ " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "�������� ������ " + this.date.getValue()));
		}
	}

	@FXML
	private void MethodSeminar(ActionEvent event) {
		if (teacher.getText().isEmpty() || about.getText().isEmpty() || aud.getText().isEmpty()
				|| time.getText().isEmpty()) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			MethodSeminar document = new MethodSeminar(this.about.getText(), this.teacher.getText(),
					Integer.valueOf(this.aud.getText()), date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "���������� ������ " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "���������� ������ " + this.date.getValue()));
		}
	}

	@FXML
	private void writeDepartmentSession(ActionEvent event) {
		if (protocol.getText().isEmpty() || about.getText().isEmpty() || aud.getText().isEmpty()
				|| time.getText().isEmpty()) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			System.out.println(this.about.getText());
			DepartmentSession document = new DepartmentSession(this.about.getText(),
					Integer.valueOf(this.protocol.getText()), writeDate.getValue(), Integer.valueOf(this.aud.getText()),
					date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "�������� ������� " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "�������� ������� " + this.date.getValue()));
		}
	}

	@FXML
	private void readDepartmentSession(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		Stage stage = new Stage();
		File file = fileChooser.showOpenDialog(stage);
		if (file.exists()) {
			try {
				this.about.setText(new DepartmentSession().readFromWord(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@FXML
	private void writeScienceConfrension(ActionEvent event) {
		if (about.getText().isEmpty() || aud.getText().isEmpty()
				|| time.getText().isEmpty()) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			System.out.println(this.about.getText());
			ScienceConferension document = new ScienceConferension(this.about.getText(),
					Integer.valueOf(this.aud.getText()),date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "����� ���� " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "����� ���� " + this.date.getValue()));
		}
	}

	@FXML
	private void readScienceConfrension(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		Stage stage = new Stage();
		File file = fileChooser.showOpenDialog(stage);
		if (file.exists()) {
			try {
				this.about.setText(new DepartmentSession().readFromWord(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@FXML
	private void CompanySeminar(ActionEvent event) {
		if (teacher.getText().isEmpty() 
				|| about.getText().isEmpty() 
				|| aud.getText().isEmpty()
				|| time.getText().isEmpty() || scienceLeader.getText().isEmpty() ) {
			this.message.setText("�� �� ���� ���������");
		} else {
			System.out.println(this.time.getText());
			LocalTime time = LocalTime.of(Integer.valueOf(this.time.getText(0, 2)),
					Integer.valueOf(this.time.getText(3, 5)));
			LocalDateTime date = LocalDateTime.of(this.date.getValue(), time);
			CompanySeminar document = new CompanySeminar(this.about.getText(), this.teacher.getText(),
					this.scienceLeader.getText(),Integer.valueOf(this.aud.getText()), date);
			document.writeToWord("C:\\Users\\Mike\\Documents\\Univer\\" + "������ ������������ �������� " + this.date.getValue());
			mainApp.addFile(new File("C:\\Users\\Mike\\Documents\\Univer\\" + "������ ������������ �������� " + this.date.getValue()));
		}
	}
	
	public void setMainApp(Main main) {
		this.mainApp = main;
	}
}
