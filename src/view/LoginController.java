package view;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.User;
public class LoginController implements Initializable{
	@FXML
	private TextField login;
	@FXML
	private PasswordField password;
	@FXML
	private Label message;
	@FXML
	private List<User> users;
	
	// Reference to the main application.
	private Main mainApp;

	/**
	 * The constructor. The constructor is called before the initialize() method.
	 */
	public LoginController() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	@FXML
	private void loginAction(ActionEvent event) throws IOException {
		for (User user : users) {
			if (login.getText().equals(user.getEmail())) {
				if (password.getText().equals(user.getPassword())) {
					(((Node) event.getSource()).getScene()).getWindow().hide();
					if (user.getRole().equals("user")) {
						mainApp.setCurrentUser(user);
						mainApp.showTeacher();
					} else {
						mainApp.setCurrentUser(user);
						mainApp.showEmail();
					}
				} else {
					message.setText("Password invalid");
				}
			} else {
				message.setText("Login invalid");
			}
		}
	}
	
	@FXML
	private void SendMessageAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
		Parent parent = FXMLLoader.load(getClass().getResource("/controller/Email.fxml"));
		Scene scene = new Scene(parent);
		Stage stage = new Stage();
		stage.setTitle("Student page");
		stage.setScene(scene);
		stage.show();
	}
	
	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
		Parent parent = FXMLLoader.load(getClass().getResource("/controller/Login.fxml"));
		Scene scene = new Scene(parent);
		Stage stage = new Stage();
		stage.setTitle("Login page");
		stage.setScene(scene);
		stage.show();
	}
	public void setMainApp(Main main) {
		this.mainApp = main;
		this.users=main.getUsers();
	}

}


	

