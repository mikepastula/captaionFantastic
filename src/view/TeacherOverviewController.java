package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.User;

public class TeacherOverviewController implements Initializable {
	@FXML
	private TableView<User> personTable;
	@FXML
	private TableColumn<User, String> firstNameColumn;
	@FXML
	private TableColumn<User, String> lastNameColumn;

	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField secondName;
	@FXML
	private TextField email;
	@FXML
	private TextField password;
	@FXML
	private CheckBox sendIt;

	private Main mainApp;

	public TeacherOverviewController() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		firstNameColumn.setCellValueFactory(cellData ->new SimpleStringProperty(cellData.getValue().getFirstname()));
		lastNameColumn.setCellValueFactory(cellData ->new SimpleStringProperty(cellData.getValue().getLastname()));

		// Clear person details.
		showPersonDetails(null);

		// Listen for selection changes and show the person details when changed.
		personTable.getSelectionModel().selectedItemProperty()
				.addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
	}

	/**
	 * Fills all text fields to show details about the person. If the specified
	 * person is null, all text fields are cleared.
	 * 
	 * @param person
	 *            the person or null
	 */
	private void showPersonDetails(User person) {
		if (person != null && person.getRole().equals("user")) {
			// Fill the labels with info from the person object.
			firstName.setText(person.getFirstname());
			lastName.setText(person.getLastname());
			secondName.setText(person.getSecondname());
			email.setText(person.getEmail());
			password.setText(person.getPassword());
			sendIt.setSelected(person.getSend());
		} else {
			// Person is null, remove all the text.
			firstName.setText("");
			lastName.setText("");
			secondName.setText("");
			email.setText("");
			password.setText("");
			sendIt.setSelected(false);
		}
	}
	
	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeletePerson() {
		int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0 ) {
			User temp =personTable.getItems().get(selectedIndex);
			if(temp.getRole().equals("user")) {
				mainApp.deleteUser(selectedIndex);
				ObservableList<User> tempUsers=FXCollections.observableArrayList(mainApp.getUsers());
				this.personTable.setItems(tempUsers);
			  }
		} else {
			System.out.println("ERROR");
		}
	}
	
	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	@FXML
	private void handleNewPerson() {
		User tempPerson = new User();
		if(firstName.getText().matches("^[\\D]+$") && lastName.getText().matches("^[\\D]+$") && secondName.getText().matches("^[\\D]+$")
				&&email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$") && !password.getText().equals("")) {
			tempPerson.setFirstname(firstName.getText());
			tempPerson.setLastname(lastName.getText());
			tempPerson.setEmail(email.getText());
			tempPerson.setSecondname(secondName.getText());
			tempPerson.setPassword(password.getText());
			tempPerson.setRole("user");
			tempPerson.setSend(true);
			mainApp.addUser(tempPerson);
			ObservableList<User> tempUsers=FXCollections.observableArrayList(mainApp.getUsers());
			this.personTable.setItems(tempUsers);
		}
		else {
			System.out.println(firstName.getText().matches("^[\\D]+$"));
			System.out.println(lastName.getText().matches("^[\\D]+$"));
			System.out.println(email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$"));
			System.out.println(!password.getText().equals(""));
		}
	}

	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
	}
	
	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	@FXML
	private void handleSave() {
		int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		User temp = mainApp.getUsers().get(selectedIndex);
		if(firstName.getText().matches("^[\\D]+$") && lastName.getText().matches("^[\\D]+$")
				&&email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$") && !password.getText().equals("")) {
			temp.setFirstname(firstName.getText());
			temp.setLastname(lastName.getText());
			temp.setEmail(email.getText());
			temp.setSecondname(secondName.getText());
			temp.setPassword(password.getText());
			temp.setRole("user");
			temp.setSend(sendIt.isSelected());
			//temp.setSend(sendIt.getText());
			mainApp.deleteUser(selectedIndex);
			mainApp.addUser(temp);
			mainApp.savePersonDataToFile();
			//ObservableList<User> tempUsers=FXCollections.observableArrayList(mainApp.getUsers());
			//personTable.setItems(tempUsers);
		}
		else {
			System.out.println(firstName.getText().matches("^[\\D]+$"));
			System.out.println(lastName.getText().matches("^[\\D]+$"));
			System.out.println(email.getText().matches("^[a-zA-Z0-9]+@[a-z]+[.][a-z]+$"));
			System.out.println(!password.getText().equals(""));
		}
	}

	public void setMainApp(Main main) {
		this.mainApp = main;
	       // Add observable list data to the table
		ObservableList<User> tempUsers=FXCollections.observableArrayList(main.getUsers());
		this.personTable.setItems(tempUsers);
	}
}
