package view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class FileOverViewController implements Initializable {
	@FXML
	private TableView<File> fileTable;
	@FXML
	private TableColumn<File, String> NameColumn;
	@FXML
	private TableColumn<File, String> PathColumn;

	private Main mainApp;

	public FileOverViewController() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 NameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
		 PathColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAbsolutePath()));

	}

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeletePerson() {
		int selectedIndex = fileTable.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			mainApp.deleteFile(selectedIndex);
			ObservableList<File> tempUsers = FXCollections.observableArrayList(mainApp.getFiles());
			this.fileTable.setItems(tempUsers);
		} else {
			System.out.println("ERROR");
		}
	}
	
	@FXML
	private void GetBackAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
	}
	
	public void setMainApp(Main main) {
		this.mainApp = main;
	       // Add observable list data to the table
		ObservableList<File> tempUsers=FXCollections.observableArrayList(main.getFiles());
		this.fileTable.setItems(tempUsers);
	}
}
