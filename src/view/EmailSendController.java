package view;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.User;

public class EmailSendController implements Initializable {
	final FileChooser fileChooser = new FileChooser();
	final String username = "testcoursepaper2017@gmail.com";
	final String password = "test1234567";
	@FXML
	private TextArea message;
	@FXML
	private TextField subject;
	@FXML
	private List<User> users;

	private List<File> files;
	
    // Reference to the main application.
    private Main mainApp;
	
	public EmailSendController() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
	}
	
	public void setMainApp(Main mainApp) {
        this.mainApp = mainApp;
        this.files = mainApp.getFiles();
        this.users = mainApp.getUsers();
    }
	
	@FXML
	private void SendEmailAction(ActionEvent event) throws IOException {

		String host = "smtp.gmail.com";
		String to = "mikepastula@gmail.com";
		String subject = "test attach file";
		String text = "test attach file";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "587");
		Session session = Session.getInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(text);
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			Multipart multipart = new MimeMultipart();

			if (files != null) {
				System.out.println("ad");
				for (File f : files) {
					messageBodyPart = new MimeBodyPart();
					DataSource source = new FileDataSource(f);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(f.getName());
					multipart.addBodyPart(messageBodyPart);
				}
				message.setContent(multipart);
			}
			message.setText(this.message.getText());

			Transport.send(message);

			System.out.println("Done");
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}

	@FXML
	private void AttachEmailAction(ActionEvent event) throws IOException {
		Stage stage = new Stage();
		List<File> files = fileChooser.showOpenMultipleDialog(stage);
		if (files!=null) {
			this.files.addAll(files);
		}
	}

	@FXML
	private void CreateAdvertisement(ActionEvent event) throws IOException {
		ChoiceDialog<String> dialog = new ChoiceDialog<String>("³������ ����", "���������� ������",
				"�������� ������", "�������� �������", "�������� ����� ����", "������� ������������ ��������",
				"���� ������ �������� ����������");
		dialog.setTitle("Create advertisement");
		dialog.setHeaderText("������� ��� ����������");
		dialog.setContentText("���������� ���:");
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(choise -> {
			switch (choise) {
			case "³������ ����": mainApp.showAdvertisement("/view/OpenLesson.fxml");	break;
			case "�������� �������":mainApp.showAdvertisement("/view/DepartmentSession.fxml");	break;
			case "������� ������������ ��������":mainApp.showAdvertisement("/view/CompanySeminar.fxml");break;
			case "�������� ����� ����":mainApp.showAdvertisement("/view/ScienceConferension.fxml");break;
			case "���������� ������":mainApp.showAdvertisement("/view/MetodSeminar.fxml");
				break;
			case "�������� ������":mainApp.showAdvertisement("/view/ScienceSeminar.fxml");
				break;
			case "���� ������ �������� ����������":mainApp.showAdvertisement("/view/OpenLesson.fxml");	break;
			}
		});
	}

	@FXML
	private void ShowTeachersOverview(ActionEvent event) throws IOException {
		mainApp.showTeacherOverview();
	}
	
	@FXML
	private void ShowFilesOverview(ActionEvent event) throws IOException {
		mainApp.showFilesOverview();
	}
	 
	@FXML
	private void ExitAction(ActionEvent event) throws IOException {
		(((Node) event.getSource()).getScene()).getWindow().hide();
		mainApp.initRootLayout();

	}
}
